# Shells

[NetMiaou](https://github.com/TRIKKSS/NetMiaou "is an crossplatform hacking tool that can do reverse shells, send files, create an http server or send and receive tcp packet") is an crossplatform hacking tool that can do reverse shells, send files, create an http server or send and receive tcp packet.

# ***Evasion***

[](https://github.com/TRIKKSS/NetMiaou#usage)[Mortar](https://github.com/0xsp-SRD/mortar) red teaming evasion technique to defeat and divert detection and prevention of security products.Mortar Loader performs encryption and decryption of selected binary inside the memory streams and execute it directly with out writing any malicious indicator into the hard-drive. Mortar is able to bypass modern anti-virus products and advanced XDR solutions,
