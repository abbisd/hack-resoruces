# Capture The Flag

[Hacker101](https://www.hacker101.com/) is a free class for web security. Whether you’re a programmer with an interest in bug bounties or a seasoned security professional, Hacker101 has something to teach you.

[TryHackMe](https://tryhackme.com/) a good starting point

[HackTheBox](https://www.hackthebox.com/) one of the most complex CTF platforms

[Root-Me](https://www.root-me.org) The fast, easy, and affordable way to train your hacking skills.

[VulnHub](https://www.vulnhub.com/) VMS built for CTF challenges.

# Write-ups

[Infosecwriteups](https://infosecwriteups.com/)  A collection of awesome write-ups from the best hackers in the worlds from topics ranging from bug bounties, CTFs, Hack the box walkthroughs, hardware challenges, real-life encounters and everything which can help other enthusiasts learn.

[Positive Security Research Blog](https://positive.security/blog)

[Zerodayinitiative](https://www.zerodayinitiative.com/blog) The Zero Day Initiative (ZDI) was created to encourage the reporting of 0-day vulnerabilities privately to the affected vendors by financially rewarding researchers. At the time, there was a perception by some in the information security industry that those who find vulnerabilities are malicious hackers looking to do harm. Some still feel that way. While skilled, malicious attackers do exist, they remain a small minority of the total number of people who actually discover new flaws in software.  

[Mitre](https://attack.mitre.org/) is a globally-accessible knowledge base of adversary tactics and techniques based on real-world observations. The ATT&CK knowledge base is used as a foundation for the development of specific threat models and methodologies in the private sector, in government, and in the cybersecurity product and service community.

# News

[TheHackerNews](https://thehackernews.com/)

[HelpNetSecurity](https://www.helpnetsecurity.com/)
